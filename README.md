
All commands are proposed to be run from the QGIS git project root folder:

```
docker run --rm -ti -v $(pwd):/cpp registry.gitlab.com/rudert-geoinformatik/qgis-dev-image:master bash -c "rm -rf /cpp/build-master && mkdir /cpp/build-master && cd /cpp/build-master && cmake -D CMAKE_BUILD_TYPE=Debug .."
```

```
docker run --rm -ti -v $(pwd):/cpp registry.gitlab.com/rudert-geoinformatik/qgis-dev-image:master bash -c "cd /cpp/build-master && make -jX clean all"
```

The following command runs QGIS Debug with GDB coverage bound to local X-Server. To use it, first open communication of X-Server (potentially insecure)

```
xhost +
```

```
docker run --rm -ti -e DISPLAY=unix$DISPLAY -e TZ='Europe/Paris' -v /tmp/.X11-unix:/tmp/.X11-unix -v $(pwd):/cpp -p 34567:34567 registry.gitlab.com/rudert-geoinformatik/qgis-dev-image:master bash -c "gdbserver 0.0.0.0:34567 /cpp/build-master/output/bin/qgis"
```

```
docker run --rm -ti -e DISPLAY=unix$DISPLAY -e TZ='Europe/Paris' -v ${HOME}/.Xauthority:/home/dev/.Xauthority:rw -v /dev/dri:/dev/dri -v /dev/snd:/dev/snd -v /tmp/.X11-unix:/tmp/.X11-unix -v $(pwd):/cpp -p 34567:34567 registry.gitlab.com/rudert-geoinformatik/qgis-dev-image:master bash -c "/cpp/build-master/output/bin/qgis"
```

use local machine tmp to ccache to be able to sync local user with user in container to build as nonroot

```
docker run --rm -ti -u $(id -u):$(id -g) -e DISPLAY=unix$DISPLAY -e TZ='Europe/Paris' -v ${HOME}/.Xauthority:/home/dev/.Xauthority:rw -v /dev/dri:/dev/dri -v /dev/snd:/dev/snd -v /tmp/.X11-unix:/tmp/.X11-unix -v $(pwd):/cpp --privileged -p 34567:34567 -v /tmp:/ccache registry.gitlab.com/rudert-geoinformatik/qgis-dev-image:master
```