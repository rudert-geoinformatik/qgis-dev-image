FROM debian:bullseye

ENV debian_frontend=noninteractive

RUN apt-get update && apt-get install -y \
    bison \
    build-essential \
    ca-certificates \
    ccache \
    cmake \
    cmake-curses-gui \
    dh-python \
    doxygen \
    expect \
    flex \
    flip \
    g++ \
    gdal-bin \
    gdb \
    git \
    graphviz \
    grass-dev \
    gdbserver \
    libexiv2-dev \
    libexpat1-dev \
    libfcgi-dev \
    libgdal-dev \
    libgeos-dev \
    libgsl-dev \
    libpdal-dev \
    libpq-dev \
    libproj-dev \
    libprotobuf-dev \
    libqca-qt5-2-dev \
    libqca-qt5-2-plugins \
    libqscintilla2-qt5-dev \
    libqt5opengl5-dev \
    libqt5serialport5-dev \
    libqt5sql5-sqlite \
    libqt5svg5-dev \
    libqt5webkit5-dev \
    libqt5xmlpatterns5-dev \
    libqwt-qt5-dev \
    libspatialindex-dev \
    libspatialite-dev \
    libsqlite3-dev \
    libsqlite3-mod-spatialite \
    libyaml-tiny-perl \
    libzip-dev \
    libzstd-dev \
    lighttpd \
    locales \
    ninja-build \
    ocl-icd-opencl-dev \
    opencl-headers \
    pandoc \
    pdal \
    pkg-config \
    poppler-utils \
    protobuf-compiler \
    pyqt5-dev \
    pyqt5-dev-tools \
    pyqt5.qsci-dev \
    python3-all-dev \
    python3-autopep8 \
    python3-dateutil \
    python3-dev \
    python3-future \
    python3-gdal \
    python3-httplib2 \
    python3-jinja2 \
    python3-lxml \
    python3-markupsafe \
    python3-mock \
    python3-nose2 \
    python3-owslib \
    python3-plotly \
    python3-psycopg2 \
    python3-pygments \
    python3-pyproj \
    python3-pyqt5 \
    python3-pyqt5.qsci \
    python3-pyqt5.qtpositioning \
    python3-pyqt5.qtsql \
    python3-pyqt5.qtsvg \
    python3-pyqt5.qtwebkit \
    python3-requests \
    python3-sip \
    python3-sip-dev \
    python3-six \
    python3-termcolor \
    python3-tz \
    python3-yaml \
    qt3d-assimpsceneimport-plugin \
    qt3d-defaultgeometryloader-plugin \
    qt3d-gltfsceneio-plugin \
    qt3d-scene2d-plugin \
    qt3d5-dev \
    qt5keychain-dev \
    qtbase5-dev \
    qtbase5-private-dev \
    qtcreator \
    qtpositioning5-dev \
    qttools5-dev \
    qttools5-dev-tools \
    rsync \
    saga \
    spawn-fcgi \
    tar \
    xauth \
    xfonts-100dpi \
    xfonts-75dpi \
    xfonts-base \
    xfonts-scalable \
    xvfb && \
  cd /usr/local/bin && \
  ln -s /usr/bin/ccache gcc && \
  ln -s /usr/bin/ccache g++

RUN git clone https://github.com/hobu/laz-perf.git /tmp/laz-perf && \
  cd /tmp/laz-perf && \
  mkdir build && \
  cd build && \
  cmake .. && \
  make

ENV CCACHE_CONFIGPATH=/ccache.conf

RUN mkdir -p /ccache/.cache/ccache/tmp && \
  chmod -R 1777 /ccache

COPY ccache.conf /ccache.conf
